$(document).ready(function(){
	class Icon extends mojs.CustomShape {
		getShape () { return '<path d="M64,39.86a17.38,17.38,0,1,0,3.31,7"></path>'; }
		getLength () { return 200; }
	};
	var parent = $('#refresh')[0];
	mojs.addShape( 'icon', Icon );
	let delay = 0,
    delayStep = 150,
    easing = 'cubic.out';
    let icon = new mojs.Shape({
		shape: 'icon',
		parent: parent,
		radius: 20,
		angle: {0 : 180},
		scale: {1 : 1.3},
		easing: mojs.easing.bezier(0.4, 0.0, 0.2, 1),
		isShowStart: true
	}).then({
		scale: 1
	});
	let burst = new mojs.Burst({
	    radius:   { 18: 40 },
	    parent: parent,
	    count: 'rand(4, 10)',
	    y: -8,
	    children: {
		    shape: ['circle', 'rect'],
		    radius: ['rand(2, 5)', 'rand(2, 5)', 'rand(2, 5)'],
		    fill: ['#FA8072', '#5F9EA0', '#5FE880'],
		    storke: ['#FA8072', '#17181A', '#5FE880'],
		    strokeWidth: 5,
	    }
    });
    let circle = new mojs.Shape({
	    shape: 'circle',
	    parent: parent,
	    fill: { '#FA8072': 'rgba(255, 255, 255, 0)' },
	    stroke: '#FA8072',
	    strokeWidth: {4 : 0},
	    radius: 20,
	    scale: {0 : 2},
	    y: -8
    });
    $(icon.el).addClass('icon');
    $(burst.el).addClass('burst');
    $(circle.el).addClass('cirlce');
	let circle1 = new mojs.Shape({
	    shape: 'circle',
	    parent: parent,
	    fill: 'none', 
	    stroke: '#886F5A',
	    strokeWidth: {8 : 0},
	    radius: 9,
	    scale: {0 : 2},
	    delay: delay += delayStep,
	    isYoyo: true,
	    x: {'-35' : -40}, y: {'-25' : -33}
    });
    let circle2 = new mojs.Shape({
	    shape: 'circle',
	    parent: parent,
	    fill: { '#F6C998': 'transparent' },
	    radius: 4,
	    scale: {0 : 3},
	    delay: delay += delayStep,
	    isYoyo: true,
	    x: {15 : 20}, y: {15 : 28}
    });
    let curve = new mojs.Shape({
	    shape: 'curve',
	    parent: parent,
	    points: 11,
	    radius: 15,
	    radiusY: 10,
	    fill: 'none',
	    scale: {0 : 1},
	    stroke: '#5FE880',
	    strokeWidth: {4 : 0},
	    angle: { 'rand(-35, -70)': 'rand(-70, -100)' },
	    delay: delay -= delayStep,
	    x: { '-15': -30 }, y: { 18: 30 },
    });
    let curve2 = new mojs.Shape({
	    shape: 'curve',
	    parent: parent,
	    points: 11,
	    radius: 15,
	    radiusY: 10,
	    fill: 'none',
	    scale: {0 : 1},
	    strokeDasharray: '100%',
	    stroke: '#5FE880',
	    angle: { 'rand(-15, -60)': '-180' },
	    strokeDashoffset: { '0' : '100%' },
	    strokeWidth: 3,
	    delay: delay -= delayStep,
	    x: {'15' : 36}, y: {'-18' : -30}
    });
    let zigzag = new mojs.Shape({
        shape: 'zigzag',
        parent: parent,
        points: 11,
        radius: 5,
        radiusX: 15,
        fill: 'none',
        stroke: {'#FAA878' : 'transparent' },
        scale: { 0 : 1.5},
        x: { '-10' : '-19' }, y: { 10 : 20 }
    });
    const timeline = new mojs.Timeline({ speed: .55 }); 
    timeline.add(icon, burst, circle, circle1, circle2, curve, curve2, zigzag);
	$('body').on('click', '#refresh', function(){
		timeline.replay();
		burst.generate();
	});
});