// Ionic Starter App

// angular.module is a global place for creating, registering and retrieving Angular modules
// 'starter' is the name of this angular module example (also set in a <body> attribute in index.html)
// the 2nd parameter is an array of 'requires'
(function(){

    var app = angular.module('starter', ['ionic']);
    app.run(function($ionicPlatform) {
        $ionicPlatform.ready(function() {
            if(window.cordova && window.cordova.plugins.Keyboard) {
                // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
                // for form inputs)
                cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);

                // Don't remove this line unless you know what you are doing. It stops the viewport
                // from snapping when text inputs are focused. Ionic handles this internally for
                // a much nicer keyboard experience.
                cordova.plugins.Keyboard.disableScroll(true);
            }
            if(window.StatusBar) {
                StatusBar.styleDefault();
            }

        });
    });
    app.factory('sesionFac', function($http, $httpParamSerializerJQLike){
        var prefix = '117400Unimag_';
        return{
            getUser: function(){
                return JSON.parse(window.localStorage.getItem(prefix+'user'));
            },
            setUser: function(u){
                window.localStorage.setItem(prefix+'user', JSON.stringify(u));
            },
            setPlan: function(p){
                window.localStorage.setItem(prefix+'plan', JSON.stringify(p));
            },
            getPlan: function(){
                return JSON.parse(window.localStorage.getItem(prefix+'plan'));
            },
            setSchedule: function(s){
                window.localStorage.setItem(prefix+'schedule', JSON.stringify(s));
            },
            getSchedule: function(){
                return JSON.parse(window.localStorage.getItem(prefix+'schedule'));
            },
            setNextSemester: function(n){
                window.localStorage.setItem(prefix+this.getUser().code+'next_semester', JSON.stringify(n));
            },
            getNextSemester: function(){
                var ns = JSON.parse(window.localStorage.getItem(prefix+this.getUser().code+'next_semester'));
                return ns ? ns : [];
            },
            addToNextSemester: function(c){
                var ns = this.getNextSemester();
                var its = false;
                for (var i = 0; i < ns.length; i++) {
                    if(ns[i].name.toLowerCase() == c.name.toLowerCase())
                        true;
                }
                if(!its){
                    ns.push(c);
                    this.setNextSemester(ns);
                }
            },
            removeFromNextSemester: function(name){
                var ns = this.getNextSemester();
                var index;
                for (var i = 0; i < ns.length; i++) {
                    if(ns[i].name.toLowerCase() == name.toLowerCase()){
                        index = i;
                        break;
                    }
                }
                ns.splice(index, 1);
                this.setNextSemester(ns);
            },
            clearUser: function(){
                window.localStorage.removeItem(prefix+'user');
            },
            prepare: function(callback){
                $http({
                    method: 'POST',
                    url: 'https://admisiones.unimagdalena.edu.co/mEstudiantes/inicio.jsp',
                    headers: {
                        'Content-Type': 'application/x-www-form-urlencoded; charset=utf-8'
                    }
                })
                .then(function(data){
                    callback(data);
                }, function(e){
                    callback(null, e);
                });
            },
            refresh: function(){
                $http({
                    method: 'POST',
                    url: 'https://admisiones.unimagdalena.edu.co/mEstudiantes/ajaxPages/insertarPaginaVisitada.jsp',
                    data: $httpParamSerializerJQLike({
                        pagina: 'https://admisiones.unimagdalena.edu.co/mEstudiantes/miHorario.jsp',
                        aler: Math.random() * 1000000
                    }),
                    headers: {
                        'Content-Type': 'application/x-www-form-urlencoded; charset=utf-8'
                    }
                })
                .then(function(data){
                }, function(e){
                });
            },
            isActive: function(callback){
                $http({
                    method: 'GET',
                    url: 'https://admisiones.unimagdalena.edu.co/mEstudiantes/misHermanos.jsp',
                    headers: {
                        'Content-Type': 'application/x-www-form-urlencoded; charset=utf-8'
                    }
                })
                .then(function(data){
                    if(data.data.search(/<script>document\.location\.href=\'\/mEstudiantes\/index.jsp\';<\/script>/) != -1)
                        callback(false);
                    else
                        callback(true);
                }, function(e){
                    callback(false);
                });
            },
            login: function(code, password, callback){
                $http({
                    method: 'POST',
                    url: 'https://admisiones.unimagdalena.edu.co/mEstudiantes/ajaxPages/validarLoginEst.jsp',
                    data: $httpParamSerializerJQLike({
                        user: code,
                        password: encode(crypto(password)),
                        alea: Math.random() * 75452
                    }),
                    headers: {
                        'Content-Type': 'application/x-www-form-urlencoded; charset=utf-8'
                    }
                })
                .then(function(data){
                    if(data.data.trim() == '1')
                        callback(data);
                    else{
                        if(data.data.trim().toLowerCase().search('incorrecta') != -1){
                            callback(null, {
                                status: 'error',
                                error: 'clave incorrecta'
                            });
                        }else if(data.data.trim() == ''){
                            callback(null, {
                                status: 'error',
                                error: 'problemas en el servidor'
                            });
                        }else{
                            callback(null, {
                                status: 'error',
                                error: 'el codigo no existe'
                            });
                        }
                    }
                }, function(e){
                    callback(null, e);
                });
            }
        }
    });
    app.factory('planFac', function(sesionFac){
        return{
            get: function(){
                return sesionFac.getPlan();
            },
            set: function(p){
                sesionFac.setPlan(p);
            },
            getCreditsOfCourse: function(name){
                return this.getCourse(name).credits;
            },
            getCourse: function(name){
                var courses = this.get().courses;
                for (var i = 0; i < courses.length; i++) {
                    if(courses[i].name.toLowerCase() == name.toLowerCase())
                        return courses[i];
                }
                if(name.toLowerCase().search(/introduccion/) != -1){
                    for (var i = 0; i < courses.length; i++) {
                        if(courses[i].name.toLowerCase().search(/introduccion/) != -1)
                            return courses[i].credits;
                    }
                }
                for (var i = 0; i < courses.length; i++) {
                    if(courses[i].name.toLowerCase().search(name.toLowerCase()) != -1 || name.toLowerCase().search(courses[i].name.toLowerCase()) != -1)
                        return courses[i];
                }
                return null;
            }
        }
    });
    app.directive("loading", function(){
        return {
            link: function(scope, elem, attr){
                scope.$watch("loading",function(newValue, oldValue) {
                    if(newValue){
                        elem.css('display', 'flex');
                        setTimeout(function(){
                            elem.css('opacity', 1);
                            $(elem).addClass('show');
                        }, 100);
                    }else{
                        setTimeout(function(){
                            if($(elem).hasClass('show')){
                                elem.css('opacity', 0);
                                setTimeout(function(){
                                    elem.css('display', 'none');
                                    $(elem).removeClass('show');
                                }, 620);
                            }
                        }, 110);
                    }
                });
            }
        };
    });
    app.directive('appVersion', function () {
        return function(scope, elm, attrs) {
            try{
                cordova.getAppVersion(function (version) {
                    elm.text(version);
                });
            }catch(e){
                console.log(e);
            }
        };
    })
    app.controller('appCtrl', ['$rootScope', '$scope', '$state', '$ionicHistory', '$interval', 'sesionFac', function($rootScope, $scope, $state, $ionicHistory, $interval, sesionFac){
        $scope.platform = ionic.Platform;
        $scope.user = undefined;
        $scope.title = 'Inicio';
        $scope.prefix = '117400Unimag_';
        $scope.user = sesionFac.getUser();
        $scope.icons = [
            'matematicas',
            'estadistica',
            'arquitectura y funcionamiento del computador',
            'economico administrativas',
            'fisica',
            'formacion en investigacion',
            'frances',
            'ingles',
            'redes'
        ];
        $scope.loading = false;
        $scope.showLoading = function(){
            $scope.loading = true;
        }
        $scope.hideLoading = function(){
            $('#refresh').removeClass('loading');
            $scope.loading = false;
        }
        $scope.isLogged = function(){
            return sesionFac.getUser() != null;
        }
        $scope.showGenericError = function(error){
            console.log(error);
            Materialize.toast('Oops, algo salio mal', 4000, 'min');
        }
        $scope.logout = function(){
            $('.drag-target').trigger('click');
            $scope.user = undefined;
            sesionFac.clearUser();
            $ionicHistory.clearCache();
            $ionicHistory.clearHistory();
            $state.go('login');
            $ionicHistory.clearCache();
            $ionicHistory.clearHistory();
        }
        $scope.refresh = function(){
            $scope.showLoading();
            $('#refresh').addClass('loading');
            $ionicHistory.clearCache();
            $state.reload();
        }
        $scope.goTo = function(path, params){
            $('.drag-target').trigger('click');
            $state.go(path, params);
        }
        $scope.server = 'https://admisiones.unimagdalena.edu.co';
        $scope.$on('$ionicView.beforeEnter', function (viewInfo, state) {
            if(state.stateName != 'schedule'){
                $('nav').removeClass('white');
            }else{
                $('body > nav').addClass('white');                
            }
            if(state.stateName != 'profile'){
                $scope.title = $state.current.data.title;
                $('nav').removeClass('no-shadow');
            }
            else{
                $scope.title = '';
                $('nav').addClass('no-shadow');
            }

            if($scope.isLogged() && state.stateName == 'login'){
                $state.go('home');
            }else if(!$scope.isLogged() && state.stateName != 'login'){
                $state.go('login');
            }
        });
        document.addEventListener('offline', function(){
            $scope.offline = true;
        }, false);
        document.addEventListener('online',  function(){
            var networkState = navigator.connection.type;
            if (networkState !== Connection.NONE) {
                $scope.offline = false;
            }else{
                $scope.offline = true;
            }
        }, false);
        $(document).ready(function(){
            $(".button-collapse").sideNav();
            $('.modal').modal();
            $('body').on('click', '.desplegable header', function(){
                var $c = $(this).next();
                if(!$c.hasClass('hide'))
                    $c.css('height', $c.height() + 'px');
                setTimeout(function(){
                    $c.toggleClass('hide');                 
                }, 200);
            });
        });
        if($scope.platform.isAndroid())
            angular.element(document.querySelector('body')).addClass('platform-android');
        if($scope.isLogged()){
            angular.element(document.querySelector('body')).addClass('isLogged');
        }
        if($scope.isLogged()){
            sesionFac.isActive(function(can){
                if(can)
                    sesionFac.refresh();
                else{
                    $scope.showLoading();
                    sesionFac.login($scope.user.code, $scope.user.password, function(data, error){
                        if(data){
                            $state.go('home', {}, {reload: true});
                            $('body').removeClass('server-problems');
                        }else{
                            if(error.error == 'problemas en el servidor')
                                $('body').addClass('server-problems');
                        }
                    });
                }
            });
        }
        $interval(function(){
            if($scope.isLogged()){
                sesionFac.isActive(function(can){
                    if(can)
                        sesionFac.refresh();
                    else{
                        sesionFac.login($scope.user.code, $scope.user.password, function(data, error){
                            if(data){
                                $scope.offline = false;
                                $state.go('home', {}, {reload: true});
                                $('body').removeClass('server-problems');                                
                            }else{
                                if(error.error == 'problemas en el servidor')
                                    $('body').addClass('server-problems');
                            }
                        });
                    }
                });
            }
        }, 30000);
        $('body').append('<script id="animationScript" type="text/javascript" src="js/animation.js"></script>');
    }]);
    app.config(['$stateProvider', '$urlRouterProvider', '$httpProvider', function($stateProvider, $urlRouterProvider, $httpProvider){
         // Enable cross domain calls
        $httpProvider.defaults.useXDomain = true;

        // Remove the header used to identify ajax call  that would prevent CORS from working
        delete $httpProvider.defaults.headers.common['X-Requested-With'];
        
        $stateProvider.state('home', {
            url: '/home',
            cache: false,
            data: {title: 'Inicio'},
            templateUrl: 'views/home.html',
            controller: 'homeCtrl',
        });
        $stateProvider.state('login', {
            url: '/login',
            cache: false,
            data: {title: 'Iniciar sesion'},
            templateUrl: 'views/login.html',
            controller: 'loginCtrl',
        });
        $stateProvider.state('schedule', {
            url: '/schedule',
            cache: false,
            data: {title: 'Horario'},
            templateUrl: 'views/schedule.html',
            controller: 'scheduleCtrl',
        });
        $stateProvider.state('plan', {
            url: '/plan',
            cache: false,
            data: {title: 'Plan'},
            templateUrl: 'views/plan.html',
            controller: 'planCtrl',
        });
        $stateProvider.state('profile', {
            url: '/profile',
            cache: false,
            data: {title: 'Perfil'},
            templateUrl: 'views/profile.html',
            controller: 'profileCtrl',
        });
        $stateProvider.state('history', {
            url: '/history',
            cache: false,
            data: {title: 'Historia'},
            templateUrl: 'views/history.html',
            controller: 'historyCtrl',
        });
        $stateProvider.state('next-semester', {
            url: '/next-semester',
            cache: false,
            data: {title: 'Siguiente semestre'},
            templateUrl: 'views/next-semester.html',
            controller: 'nextSemesterCtrl',
        });
         $stateProvider.state('enrollment', {
            url: '/enrollment',
            cache: false,
            data: {title: 'Matricula'},
            templateUrl: 'views/enrollment.html',
            controller: 'enrollmentCtrl',
        });
        $stateProvider.state('about', {
            url: '/about',
            cache: false,
            data: {title: 'Acerca de'},
            templateUrl: 'views/about.html',
            controller: 'aboutCtrl'
        });
        if(window.localStorage.getItem('117400Unimag_') != undefined)
            $urlRouterProvider.otherwise('/home');
        else
            $urlRouterProvider.otherwise('/login');
    }]);
    function crypto(text){
        return hex_md5(text).toUpperCase();
    }
    function encode(str) {
        var result = "";
        for (i = 0; i < str.length; i++) {
            if (str.charAt(i) == " ") result += " ";
            else result += str.charAt(i);
        }
        result = escape(result);
        while(result.indexOf("%F1") != -1 || result.indexOf("%D1") != -1 || result.indexOf("%A1") != -1){
            result = result.replace("%F1","ï¿½");
            result = result.replace("%D1","ï¿½");
            result = result.replace("%A1","ï¿½");
            
        }
        return result;
    }
})();
function sortArrayBy(array, prop, descend) {
    array.sort(function (a, b) {
        return (a[prop] > b[prop]) ? 1 : ((a[prop] < b[prop]) ? -1 : 0);
    });
    if (descend)
        array.reverse();
    return array;
}
 function getHourPretty(h){
    if(h > 12)
        return h - 12 + 'p';
    else if(h < 12)
        return h + 'a';
    else
        return '12p';
}