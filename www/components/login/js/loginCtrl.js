angular.module('starter').controller('loginCtrl', ['$rootScope', '$scope', '$http', '$state', '$ionicHistory', 'sesionFac', '$httpParamSerializerJQLike', '$interval', 'planFac', function($rootScope, $scope, $http, $state, $ionicHistory, sesionFac, $httpParamSerializerJQLike, $interval, planFac){
	$scope.data = {};
	$scope.sending = false;
	var afterLogin;
	$scope.$on('$ionicView.loaded', function (viewInfo, state) {
		if(state.stateName == 'login'){
			$('form[name="loginForm"] input').on('keyup', function(e){
				if(e.keyCode == 13)
					$scope.login(e);
			});
		}
	});
	$scope.login = function($event){
		// peticion al api
		$scope.loginForm.$setSubmitted();
		if(!$scope.sending && $scope.loginForm.$valid){
			$scope.sending = true;
			$('#login').addClass('loading');
			sesionFac.login($scope.data.code, $scope.data.password, function(data, error){			
				console.log(data);
				$scope.sending = false;
				setTimeout(function(){
					$('#login').removeClass('loading');
				}, 2000);
				if(error){
					$scope.sending = false;
					if(error.error == 'clave incorrecta')
						Materialize.toast('La contraseña es incorrecta', 4000, 'min');
					else
						Materialize.toast('Ese codigo no exise', 4000, 'min');
					console.error(error);
				}else{
					$scope.showLoading();
					$http({
						method: 'GET',
						url: $scope.server + '/mEstudiantes/inicio.jsp',
						headers: {
							'Content-Type': 'application/x-www-form-urlencoded; charset=utf-8'
						} 
					}).then(function(data){
						var html = data.data.replace(/img/g, 'pato');
						var t = $(html).find('ul.nav.navbar-nav.navbar-right a:first').text().trim();
						var name = t.match(/- .+/)[0].replace(/- /,'').toLowerCase();
						getPlan(function(plan, error){
							console.log(plan);
							planFac.set(plan);
							getWeightedAverage(function(average, officialAverage){
								var img = 'https://admisiones.unimagdalena.edu.co/ayreAdmin/mhe/ajaxPages/fa_downFoto.jsp?id=' + $scope.data.code;
								var u = {
									name: name,
									img: img,
									code: $scope.data.code,
									password: $scope.data.password,
									officialAverage: officialAverage,
									average: average
								};
								console.log(u);
								sesionFac.setUser(u);
								$scope.$parent.user = u;
								$ionicHistory.clearCache();
								$ionicHistory.clearHistory();
								$state.go('home', {}, {reload: true});
								$ionicHistory.clearCache();
								$ionicHistory.clearHistory();
								$scope.hideLoading();
							});
						});
					}, $scope.showGenericError);
				}
			}, $scope.showGenericError);
		}
	};
	function getPlan(callback){
		sesionFac.prepare(function(){
			$http({
				method: 'GET',
				url: $scope.server + '/mEstudiantes/miPlanDeEstudio.jsp'
			})
			.then(function(data){
				var html = data.data.replace(/img/g, 'pato');						
				var $info = $(html).find('#infoPlan');
				var name = $info.find('tr:first-child td:first-child')[0].childNodes[2].nodeValue.trim();
				var director = $info.find('tr:first-child td:last-child')[0].childNodes[2].nodeValue.trim();
				var code = $info.find('tr:nth-child(2) td:last-child').text().trim().match(/[0-9]+.+/)[0];
				var credits, courses_num, semesters_num, separation;
				var courses = [];
				if($info.find('tr:nth-child(3) td').text().search('Total Semestres') != -1){
					// separadas por semestres
					separation = 'semester';
					var numeros = $info.find('tr:nth-child(3) td b');
					semesters_num = $(numeros[0]).text().trim();
					credits = $(numeros[1]).text().trim();
					courses_num = $(numeros[2]).text().trim();
					$(html).find('#secciones > table#asignaturas tr[data-toggle="tooltip"]').each(function(i, tr){
						var semester;
						try{
							var prev = $(tr).prevAll('tr:not([data-toggle])');
							semester = $(prev[0]).find('td')[0].childNodes[1].nodeValue.trim();
						}catch(e){
							console.info($(tr).prevAll('tr:not([data-toggle])'));
							console.info($(tr));
						}
						var $tds = $(tr).children();
						courses.push({
							semester: semester,
							code: $($tds[0]).text().trim(),
							name: $($tds[1]).text().trim(),
							credits: $($tds[2]).text().trim(),
							required: $($tds[3]).text().trim().toLowerCase().search('s') != -1
						});
					});
				}else{
					// materias separadas por componentes y areas
					separation = 'component';
					var numeros = $info.find('tr:nth-child(3) td b');
					credits = $(numeros[0]).text().trim();
					courses_num = $(numeros[1]).text().trim();
					$(html).find('#secciones > table.upHeadCompBorders').each(function(i, t){
						var area = $(t).find('tr:first-child td:last-child').text().trim().toLowerCase();
						var component = $(t).find('tr:last-child td:last-child').text().trim().toLowerCase();
						$(t).next('.styleTablePlan').find('tbody tr').each(function(i, tr){
							var $tds = $(tr).children();
							courses.push({
								area: area,
								component: component,
								code: $($tds[0]).text().trim(),
								name: $($tds[1]).text().trim(),
								credits: $($tds[2]).text().trim(),
								required: $($tds[3]).text().trim().toLowerCase().search('s') != -1
							});
						});
					});
				}
				var plan = {
					name: name,
					separation: separation,
					director: director,
					code: code,
					credits: credits,
					semesters_num: semesters_num,
					courses_num: courses_num,
					courses: courses
				};
				callback(plan);
			}, function(e){
				callback(null, e);
			});
		});
	}
	function getWeightedAverage(callback){
		$http({
			method: 'GET',
			url: $scope.server + '/mEstudiantes/miHoja.jsp'
		})
		.then(function(data){
			var html = data.data.replace(/img/g, 'pato');
			var grades = 0;
			var credits = 0;
			var officialAverage = 0;
			$(html).find('#asignaturas tr').each(function(i, tr){
				var $tr = $(tr);
				if($tr.find('th').length == 0 && $tr.find('td').length > 2){
					var c = parseInt(planFac.getCreditsOfCourse($tr.find('td:nth-child(2)').text().trim()));
					if(c > 0){
						grades += parseInt($tr.find('td:nth-child(3)').text().trim()) * c;
	    				credits += c;
	    			}
				}else if($tr.find('td').length == 2){
					officialAverage = $tr.find('td:nth-child(2)').text().match(/[0-9]+/)[0].trim();
				}
			});
			callback(Math.round(grades/credits), officialAverage);
		}, function(e){
			callback(null, e);
		});
	}
	function getAfterLogin(){
		$http({
			method: 'GET',
			url: $scope.server + '/mEstudiantes/miPlanDeEstudio.jsp'
		})
		.then(function(data){
			var html = data.data;
			afterLogin = html.match(/var afterLogin = ".+\?.+";/)[0].replace(/var afterLogin = ".+\?/, '').replace(/";/, '');
			console.log(afterLogin);
		});
	}
	function toBase64(src, callback){
		var img = new Image;
		img.onload = function () {
			var canvas = document.createElement('canvas');
			canvas.width = img.width;
			canvas.height = img.height
			try{
				var ctx = canvas.getContext('2d');
				ctx.drawImage(img, 0, 0);
				var format = 'image/png';
				var dataURL = canvas.toDataURL(format);
				callback(dataURL);
			} catch (e) {
				console.error(e);
				callback(src);
			}
		};
		img.src = src;
	}
}]);