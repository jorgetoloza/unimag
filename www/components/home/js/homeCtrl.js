angular.module('starter').controller('homeCtrl', ['$rootScope', '$scope', '$http', '$state', '$ionicPlatform', 'sesionFac', 'planFac', function($rootScope, $scope, $http, $state, $ionicPlatform, sesionFac, planFac){
	$scope.courses = [];
    $scope.not_yet = false;
    $scope.$on('$ionicView.loaded', function (viewInfo, state) {
        if(state.stateName == 'home' && $scope.isLogged()){
            $scope.showLoading();
        	getGrades(function(courses, error){
                if(error == 'no hay horario'){
                    $scope.not_yet = true;
                }else{
                    $scope.courses = courses;
                }
                $scope.hideLoading();
            });
		}
	});
	function getGrades(callback){
		sesionFac.prepare(function(){
            $http({
                method: 'GET',
                url: $scope.server + '/mEstudiantes/miNotas.jsp',
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded; charset=utf-8'
                }
            })
            .then(function(data){
                var html = data.data.replace(/img/g, 'pato');
                if($(html).find('#horarioAct')[0] && html.toLowerCase().search('no tiene horario') == -1){
                    var $trs = $(html).find('#horarioAct tr:not(:first-child)');
                    var courses = [];
                    $trs.each(function(i, tr){
                    	var name = $(tr).children('td:nth-child(2)').text().trim();
                    	var recovery = $(tr).children('td:nth-child(7)').text().trim();
                    	var total = $(tr).children('td:nth-child(6)').text().trim();
                    	total = recovery != '-' ? total * 0.3 + recovery * 0.7 : total;
                    	var steps = [];
                    	for (var j = 3; j < 6; j++) {
                    		var s = $(tr).children('td:nth-child(' + j + ')').text().trim();
                    		if(s != '-')
                    			steps.push(s);
                    	}
                    	var disapproved;
                    	if(steps.length == 3){
                    		if(total < 300)
                    			disapproved = true;
                    		else
                    			disapproved = false;
                    	}
                    	var img, component;
                    	if(planFac.get().separation == 'component'){
                            component = planFac.getCourse(name).component;
                    		img = ($scope.icons.indexOf(component) != -1 ? component : 'default') + '.svg';
                		}	
                    	courses.push({
                    		name: name,
                    		component: component,
                            recovery: (recovery != '-' ? recovery : null),
                    		img: img,
                    		type: i,
                    		steps: steps,
                    		total: total,
                    		disapproved: disapproved
                    	});
                    });
                    callback(courses);
                }else if(html.toLowerCase().search('no hay horario')){
                    callback(null, 'no hay horario');
                }
                
            }, function(e){
                callback(null, e);
            });
        });
	}
}]);