angular.module('starter').controller('scheduleCtrl', ['$scope', '$rootScope', '$state', '$http', 'sesionFac', '$timeout', function($scope, $rootScope, $state, $http, sesionFac, $timeout){
    $scope.not_yet = false;
    
    $scope.days = [
        {
            name: 'lunes',
            events_number: 0
        },
        {
            name: 'martes',
            events_number: 0
        },
        {
            name: 'miercoles',
            events_number: 0
        },
        {
            name: 'jueves',
            events_number: 0
        },
        {
            name: 'viernes',
            events_number: 0
        },
        {
            name: 'sabado',
            events_number: 0
        },
        {
            name: 'domingo',
            events_number: 0
        }
    ];
    $scope.hours = [
        '6a',
        '7a',
        '8a',
        '9a',
        '10a',
        '11a',
        '12p',
        '1p',
        '2p',
        '3p',
        '4p',
        '5p',
        '6p',
        '7p',
        '8p',
        '9p',
        '10p'
    ];
    $scope.courses = [];
    $scope.events = [];
    $scope.$on('$ionicView.loaded', function (viewInfo, state) {
        if(state.stateName == 'schedule'){
            $('body > nav').addClass('white');
            $scope.showLoading();
            getSchedule(function(events){
                if(events){
                    $scope.not_yet = false;
                    $scope.events = events;
                    $('.vista[name="schedule"] ul.tabs').tabs();                
                    for (var j = 0; j < $scope.events.length; j++) {
                        var e = $scope.events[j];
                        for (var d = 0; d < $scope.days.length; d++) {
                            if($scope.days[d].name == e.day){
                                $scope.days[d].events_number += 1;
                            }
                        }
                        var $h = $('.hours[id="' + e.day + '"] .hour[name="' + e.initial_date + '"]');
                        var $html = $(
                            '<div class="event" data-name="' + e.course.name + '" data-type="' + e.course.type + '">' +
                                '<span class="place">' + e.place + '</span>' +
                                '<span class="name">' + e.course.name + (e.course.teacher ? ' • ' + e.course.teacher : '') + '</span>' +
                            '</div>'
                        );
                        var i = e.initial_date;
                        var f = e.finish_date;
                        if(i.match(/a/) && f.match(/p/) && f != '12p'){
                            f = f.replace(/p|a/, '');
                            f = parseInt(f) + 12;
                        }else if(i.match(/p/) && f == '12p'){

                        }
                        i = i.replace(/p|a/, '');
                        f = typeof f == 'string' ? f.replace(/p|a/, '') : f;
                        var h = $('.hours:first .hour:first').height() % 10 != 0 ? $('.hours:first .hour:first').height() + 1 : $('.hours:first .hour:first').height();
                        var height = h * e.hours;
                        $html.css('height', height + 'px');
                        $h.append($html);
                    }
                    $scope.hideLoading();
                    $timeout(function(){
                        $('.vista[name="schedule"] ul.tabs').tabs({
                            swipeable: false
                        });
                        var d = new Date();
                        var $d = $('.vista[name="schedule"] ul.tabs .tab[name="' + $scope.days[d.getDay()-1].name + '"]');
                        if($d[0])
                            $d.find('a').trigger('click');
                        else
                            $('.vista[name="schedule"] ul.tabs .tab:first-child a').trigger('click');
                    }, 1000);
                }else{
                    $scope.not_yet = true;
                    $scope.hideLoading();
                }
            });
        }
    });
    var numeros;
    function getSchedule(callback){
        sesionFac.prepare(function(){
            $http({
                method: 'GET',
                url: $scope.server + '/mEstudiantes/miHorario.jsp' + (numeros ? '?' + numeros : ''),
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded; charset=utf-8'
                }
            })
            .then(function(data){
                var events = [];
                var html = data.data.replace(/img/g, 'pato');                
                var $rows = $(html).find('#primera ~ .row:first #horarioAct tbody tr.horario1');
                var url = $(html).find('a[href^="miHorario.jsp"]').attr('href');
                if(url)
                    numeros = url .replace(/miHorario.jsp\?/, '');
                if($rows[0]){
                    $rows.each(function(i, tr){
                        var $tds = $(tr).children();
                        var course = {
                            name: $($tds[0]).text(),
                            teacher: $($tds[1]).text().trim() == '-' ? null : $($tds[1]).text().trim(),
                            group: $($tds[2]).text(),
                            type: i,
                            row_number: i
                        };
                        $scope.courses.push(course);
                    });
                    $rows = $(html).find('#primera ~ .row:last #horarioAct tbody tr.horario1');
                    $rows.each(function(i, tr){
                        $(tr).children().each(function(j, td){
                            if($(td).text().trim() != '-'){
                                if($(td)[0].childNodes[0].nodeValue.search(/\//) != -1){
                                    var date1 = $(td)[0].childNodes[0].nodeValue.trim().match(/.+\n\//)[0].replace(/\/|\n| /g, '');
                                    var date2 = $(td)[0].childNodes[0].nodeValue.trim().match(/\/.+/)[0].replace(/\/|\n| /g, '');
                                    var initial_date1 = date1.replace(/-[0-9]+/, '').replace(/ /g, '');
                                    var initial_date2 = date2.replace(/-[0-9]+/, '').replace(/ /g, '');

                                    var finish_date1 = date1.replace(/[0-9]+-/, '').replace(/ /g, '');
                                    var finish_date2 = date2.replace(/[0-9]+-/, '').replace(/ /g, '');

                                    var hours1 = finish_date1 - initial_date1;
                                    var hours2 = finish_date2 - initial_date2;

                                    var place1 =  $(td)[0].childNodes[2].nodeValue.match(/.+\//)[0].trim().replace(/\//, '');
                                    var place2 =  $(td)[0].childNodes[2].nodeValue.match(/\/.+/)[0].trim().replace(/\//, '');
                                    events.push({
                                        course: getcourseByRow(i),
                                        day: $scope.days[j].name,
                                        hours: hours1,
                                        initial_date: getHourPretty(initial_date1),
                                        finish_date: getHourPretty(finish_date1),
                                        place: place1
                                    });
                                    events.push({
                                        course: getcourseByRow(i),
                                        day: $scope.days[j].name,
                                        hours: hours2,
                                        initial_date: getHourPretty(initial_date2),
                                        finish_date: getHourPretty(finish_date2),
                                        place: place2
                                    });
                                }else{
                                    var initial_date = $(td)[0].childNodes[0].nodeValue.replace(/ - [0-9]+/, '').replace(/ /g, '');
                                    var finish_date = $(td)[0].childNodes[0].nodeValue.replace(/[0-9]+ - /, '').replace(/ /g, '');
                                    var hours = finish_date - initial_date;
                                    events.push({
                                        course: getcourseByRow(i),
                                        day: $scope.days[j].name,
                                        hours: hours,
                                        initial_date: getHourPretty(initial_date),
                                        finish_date: getHourPretty(finish_date),
                                        place: $(td)[0].childNodes[2].nodeValue
                                    });
                                }
                            }
                        });
                    });
                    sesionFac.setSchedule(events);
                    callback(events);
                }else if(html.toLowerCase().search('no hay horario')){
                    var s = sesionFac.getSchedule();
                    callback(s);
                }
            }, function(e){
                var s = sesionFac.getSchedule();
                if (s) {
                    callback(s);
                }else{
                    callback(null, e);
                    $scope.showGenericError();
                }
            });
        });
    }
    function getcourseByRow(num){
        for (var i = 0; i < $scope.courses.length; i++) {
            if($scope.courses[i].row_number == num)
                return $scope.courses[i];
        }
        return null;
    }
}]);