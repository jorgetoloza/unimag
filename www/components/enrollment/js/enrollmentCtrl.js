angular.module('starter').controller('enrollmentCtrl', ['$scope', '$http', '$state', 'sesionFac', 'planFac', '$httpParamSerializerJQLike', '$interval', '$timeout', function($scope, $http, $state, sesionFac, planFac, $httpParamSerializerJQLike, $interval, $timeout){
	$scope.semester = {courses: []};
	$scope.days = [
        {
            name: 'lunes',
            events_number: 0
        },
        {
            name: 'martes',
            events_number: 0
        },
        {
            name: 'miercoles',
            events_number: 0
        },
        {
            name: 'jueves',
            events_number: 0
        },
        {
            name: 'viernes',
            events_number: 0
        },
        {
            name: 'sabado',
            events_number: 0
        },
        {
            name: 'domingo',
            events_number: 0
        }
    ];
	var activeCourse;
	$scope.$on('$ionicView.loaded', function (viewInfo, state) {
        if(state.stateName == 'enrollment'){
        	try{
            	updateTempSchedule();
            }catch(e){

            }
		}
	});
	
	$('body').on('click', '#closeModalCourses', function(){
		$('#modalCourses').css('opacity', '0');
		$timeout(function(){
			$('#modalCourses').css('display', 'none');
			$('#modalCourses').css('transform', 'scale(0.9)');
			$('#refresh').removeClass('ocultar');
			$('#closeModalCourses').addClass('ocultar');
		}, 100);
	});
	$scope.openCourses = function(course){
		$('#modalCourses').css('display', 'block');
		$scope.$parent.title = 'Materias disponibles';
		$('#refresh').addClass('ocultar');
		if($('#closeModalCourses')[0] == undefined){
			$('body').append('<div id="closeModalCourses" class="ocultar"><svg class="icon" viewBox="0 0 24 24"><path d="M19,6.41L17.59,5L12,10.59L6.41,5L5,6.41L10.59,12L5,17.59L6.41,19L12,13.41L17.59,19L19,17.59L13.41,12L19,6.41Z" /></svg></div>');
		} 
		$timeout(function(){
			$('#modalCourses').css('opacity', '1');
			$('#modalCourses').css('transform', 'scale(1)');
			$('#closeModalCourses').removeClass('ocultar');
            $scope.showLoading();
		}, 100);
		if(course){
			var id = $('#semester .course[data-name="' + course + '"]').attr('data-id');
			$scope.courses = [{name: course, id: id}];
			$timeout(function(){
				$scope.toggle(null, $('#modalCourses .groups-container:first'));
	            $scope.hideLoading();
			}, 200);
		}else{
			getCourses(function(data, error){
				if(error){
					$scope.showGenericError(error);
				}
				else{
					$scope.courses = data;
				}
	            $scope.hideLoading();
			});
		}
	};
	function updateTempSchedule(html, callback){
		$scope.showLoading();
		var credits = 0;
        getTempSchedule(html, function(courses, canSave, e){
        	if(e){
        		$scope.showGenericError(e);
        	}else{
        		$scope.semester.courses = courses;
	            for (var i = 0; i < $scope.semester.courses.length; i++) {
	            	credits += parseInt($scope.semester.courses[i].credits);
	            }
	            $scope.semester.credits = credits;
	        }
	        if(canSave)
	        	$('#save').addClass('show');
	        else
	        	$('#save').removeClass('show');
	        $('#closeModalCourses').trigger('click');
	        if(callback)
	        	callback(canSave);
	        $timeout(function(){
	        	$scope.hideLoading();
	        }, 1000);
        });
	}
	function getTempSchedule(html, callback){
		if(html){
			toJSON(html);
		}else{
		$http({
			method: 'POST',
			url: $scope.server + '/mEstudiantes/horarioTemporal',
			data: $httpParamSerializerJQLike({
				codEst: $scope.user.code
			}),
			headers: {
		    	'Content-Type': 'application/x-www-form-urlencoded; charset=utf-8'
			    }
			})
	        .then(function(data){
	            var html = data.data.replace(/img/g, 'pato');
	            toJSON(html);
	        }, function(e){
		        callback(null, null, e);
	        });
	    }
        function toJSON(html){
        	var courses = [];
	        var canSave = false;
        	try{
	        	$($(html)[0]).find('tr').each(function(i, tr){            
		        	if(i > 0){
		        		var c = {
		        			type: $(tr).find('td:nth-child(1)').text().trim(),
		        			name: $(tr).find('td:nth-child(2)').text().trim(),
		        			teacher: $(tr).find('td:nth-child(3)').text().trim(),
		        			group: $(tr).find('td:nth-child(4)').text().trim(),
		        			id: $(tr).find('td:nth-child(12) pato').attr('onclick').match(/\'[^\']+\'/)[0].replace(/\'/g, ''),
		        			events: []
		        		};
		        		c.credits = planFac.getCreditsOfCourse(c.name);
		        		$(tr).find('td').each(function(j, td){  
		        			if(j > 3 && j < 10){	
		        				if($(td).text().search(/[0-9]/) != -1){
		        					if($(td).attr('style') && $(td).attr('style').search('#900') != -1)
		        						c.crossing = true;
		        					var e = {};
		        					var text = $(td).text().trim();
		        					var start = text.match(/[0-9]+/g)[0];
	        						var finish = text.match(/[0-9]+/g)[1];
	        						var icon = start > 17 || finish > 17 ? 'night' : 'normal';
	        						if(start < 8)
	        							icon = 'morning';
	        						e = {
	        							dayIndex: j - 4,
	        							start: getHourPretty(start),
	        							finish: getHourPretty(finish),
	        							icon: icon
	        						};
	        						e.day = $scope.days[e.dayIndex].name;
	        						e.info = e.day + ' ' + e.start + ' - ' + e.finish;
		        					c.events.push(e);
		        				}
		        			}
		        		});
		        		courses.push(c);
		        	}
		        });
		        if($(html).find('#finish2:last').attr('disabled') != 'disabled')
		        	canSave = true;
	        }catch(e){
		        console.error(e);
		    }
	        callback(courses, canSave);
        }
	}
	function getCourses(callback){
		// no necesita un sesion (no hay que llamar a prepare)
		enable(function(html){
			var d = new Date();
			var nmat;
			try{
				nmat = $(html).find('#bt_agregar').attr('onClick').match(/,[^,]+,/)[0].replace(/,/g, '');
			}catch(e){
				console.log(e);
				$scope.showGenericError(e);
			}
			$http({
				method: 'POST',
				url: $scope.server + '/mEstudiantes/listarAsigAdd',
				data: $httpParamSerializerJQLike({
					codEst: $scope.user.code,
					nmat: nmat,
					anio: d.getFullYear(),
					sem: d.getMonth() > 5 ? 'II' : 'I',
					ct: '-'
				}),
				headers: {
			    	'Content-Type': 'application/x-www-form-urlencoded; charset=utf-8'
			    }
			})
	        .then(function(data){
	            var html = data.data.replace(/img/g, 'pato');
	            var courses = [];
	            try{
	            	if(html.toLowerCase().search('materias perdidas') != -1){
	            		Materialize.toast('Primero debes matricular estas', 4000, 'min');
	            	}
	            	$(html).find('a[onclick]').each(function(i, c){
	            		courses.push({
	            			id: $(c).attr('onclick').match(/\'[^\']+\'/)[0].replace(/\'/g, ''),
	            			name: $(c).text().trim()
	            		});
	            	});
					
					callback(sortArrayBy(courses, 'name'));
				}catch(e){
					callback(null, e);				
				}
	        }, function(e){
				callback(null, e);
	        });
		});
	}
	function enable(callback){
		// habilita la matricula academica así no este habilitado para ese horario
		$http({
			method: 'POST',
			url: $scope.server + '/mEstudiantes/matriculaAcademica.do',
			data: $httpParamSerializerJQLike({
				codEstHd: $scope.user.code,
				'354.0921820084251': '0.14503303271993362'
			}),
			headers: {
		    	'Content-Type': 'application/x-www-form-urlencoded; charset=utf-8'
		    }
		})
        .then(function(data){
            var html = data.data.replace(/img/g, 'pato');
            callback(html);
        }, function(e){
        	$scope.showGenericError(e);
        	callback(e);
        });
	}
	$scope.toggle = function($event, $element){
		if($element == undefined){
			$event.preventDefault();
			$event.stopPropagation();
			$element = $($event.currentTarget).closest('.groups-container')
		}
		var id = $element.attr('data-id');
        var $c = $element.find('.groups');
        if(!$element.hasClass('loading')){
	        if($c.hasClass('hide')){
		        $scope.updateGroups(id, function(){
		        	updateHeight(id);
		        	activeCourse = id;
			        $timeout(function(){
			            $c.removeClass('hide');                 
			        }, 100);
			    });
		    }else{
		    	$c.addClass('hide');
		    	var $opened = $('#modalCourses .groups-container .groups:not(.hide):first');
		    	if($opened[0])
		    		activeCourse = $opened.attr('data-id');
		    	else
		    		activeCourse = null;
		    }
		}
    };
    function updateHeight(id){
		var	$c = $('#modalCourses .groups-container[data-id="' + id + '"] .groups');
        var alto = 107; // alto de los grupos
        var altoDias = 22; // alto de los dias (martes 8-10, miercoles 7-9)

    	for (var i = 0; i < $scope.courses.length; i++) {
			if($scope.courses[i].id == id){
				var gl = $scope.courses[i].groups.length; // total de grupos
				var dl = 0; // total de dias (martes 8-10, miercoles 7-9)
				for (var j = 0; j < $scope.courses[i].groups.length; j++) {
					if($scope.courses[i].groups[j].events.length == 2 || $scope.courses[i].groups[j].events.length == 1)
						dl += 1; // una linea
					else if($scope.courses[i].groups[j].events.length > 2){
						if($(window).width() > 419)
							dl += 1; // una linea
						else
							dl += 2; // dos lineas
					}
				}
				var total = (gl * alto) + (dl * altoDias);
				$c.css('height', total + 'px');
				break;
			}
		}
    }
	$scope.updateGroups = function(id, callback){
        $('#modalCourses .groups-container[data-id="' + id + '"]').addClass('loading');
		$http({
			method: 'POST',
			url: $scope.server + '/mEstudiantes/listarGruposChg',
			data: $httpParamSerializerJQLike({
				jObj: id,
				ct: '-',
				opt: 'ins'
			}),
			headers: {
		    	'Content-Type': 'application/x-www-form-urlencoded; charset=utf-8'
		    }
		})
        .then(function(data){
            var html = data.data.replace(/img/g, 'pato');
            var groups = [];
            try{
            	if(html.toLowerCase().search('no hay grupos') != -1 || $(html).find('tr').length == 2){
            		for (var i = 0; i < $scope.courses.length; i++) {
            			if($scope.courses[i].id == id){
            				$scope.courses[i].groups = [];
            				if(planFac.getCourse($scope.courses[i].name).required)
            					$('#modalCourses .groups-container[data-id="' + id + '"]').addClass('full');
            				else
            					$('#modalCourses .groups-container[data-id="' + id + '"]').addClass('empty');

            				break;
            			}
            		}
            	}else{
	            	$(html).find('tr').each(function(i, tr){
	            		if(i > 0 && $(tr).children().length > 1){
	            			var groupId = $(tr).find('td:nth-child(2) a').attr('href').match(/\'[^\']+\'/)[0].replace(/\'/g, '');
	            			var teacher = $(tr).find('td:nth-child(2) a')[0].childNodes[2].nodeValue.match(/\([^\)]+\)/)[0].replace(/\(|\)/g, '').trim();
	            			var subTitle;
	            			for (var i = 0; i < $scope.courses.length; i++) {
            					if($scope.courses[i].id == id && $(tr).find('td:nth-child(2) a')[0].childNodes[0].nodeValue.trim().toLowerCase() != $scope.courses[i].name.toLowerCase()){
	            					subTitle = $(tr).find('td:nth-child(2) a')[0].childNodes[0].nodeValue.trim();
            					}
            				}
	            			var g = {
	            				id: groupId,
	            				teacher: teacher != '-' ? teacher : null,
	            				sub_title: subTitle,
		            			number: $(tr).find('td:first-child').text().trim(),
		            			max: $(tr).find('td:nth-child(3)').text().trim(),
		            			enrolled: $(tr).find('td:nth-child(4)').text().trim(),
		            			events: []
	            			};
	            			if($('#semester .course[data-id="' + id + '"]')[0]){
	            				// si ya tienes esa materia
	            				var num = $('#semester .course[data-id="' + id + '"] .group').text().match(/[0-9]+/)[0];
	            				// si el grupo actual es el mismo grupo en el que estas matriculado
	            				if(g.number == num)
	            					g.here = true;
	            			}
	            			// buscando los horarios
	            			$($(tr).find('td:nth-child(5)')[0].childNodes).each(function(j, c){
	            				if(c.nodeType == 3){ // si es texto
	            					var text = c.nodeValue.trim().toLowerCase().replace(/á/gi, 'a').replace(/é/gi, 'e').replace(/í/gi, 'i').replace(/ó/gi, 'o').replace(/ú/gi, 'u');
	            					var day = text.match(/[a-z]+/)[0];
	            					var start, finish, night;
	            					var e = {};
	            					if(c.nodeValue.trim().search('/') == -1){
	            						if(text.toLowerCase().search(/no tiene/) != -1){
	            							start = null;
	            							finish = null;
	            						}else{
	            							start = text.match(/[0-9]+/g)[0];
	            							finish = text.match(/[0-9]+/g)[1];
	            							var icon = start > 17 || finish > 17 ? 'night' : 'normal';
		            						if(start < 8)
		            							icon = 'morning';
	            						}
	            						e = {
	            							day: day,
	            							start: start ? getHourPretty(start) : start,
	            							finish: finish ? getHourPretty(finish) : finish,
	            							icon: icon ? icon : 'normal'
	            						};
	            						if(text.toLowerCase().search(/no tiene/) != -1)
	            							e.info = 'No tiene';
	            						else
	            							e.info = day + ' ' + e.start + ' - ' + e.finish;
	            					}else{
	            						e.info = text;
	            					}
	            					if(g.here == undefined){
		            					for (var i = 0; i < $scope.semester.courses.length; i++) {
		            						for (var j = 0; j < $scope.semester.courses[i].events.length; j++) {
		            							if($scope.semester.courses[i].events[j].info.toLowerCase() == e.info.toLowerCase())
		            								g.crossing = true;
		            						}
		            					}
		            				}
	            					g.events.push(e);
	            				}
	            			});
		            		groups.push(g);
	            		}
	            	});
	            	console.log(groups);
	            	for (var i = 0; i < $scope.courses.length; i++) {
            			if($scope.courses[i].id == id){
            				$scope.courses[i].groups = groups;
            				$('#modalCourses .groups-container[data-id="' + id + '"]').removeClass('full, empty');
            				break;
            			}
            		}
				}
			}catch(e){
				$scope.showGenericError(e);
			}
			$timeout(function(){
        		$('#modalCourses .groups-container[data-id="' + id + '"]').removeClass('loading');			
        	}, 2000);
			if(callback)
				callback();
        }, function(e){
        	$timeout(function(){
        		$('#modalCourses .groups-container[data-id="' + id + '"]').removeClass('loading');			
        	}, 2000);
			$scope.showGenericError(e);
			if(callback)
				callback();
        });
	};
	$scope.addToSemester = function(id, name, callback){
		var update = false;
		if($('#semester .course[data-name="' + name + '"]')[0]){
			// si ya tienes esa materia
			update = true;
		}
		$http({
			method: 'POST',
			url: $scope.server + '/mEstudiantes/updAsigTmp',
			data: $httpParamSerializerJQLike({
				jObj: id,
				//ct: '0',
				//opt: 'ins',
				extraCP: '0',
        		opt: update ? 'upd' : 'ins'
			}),
			headers: {
		    	'Content-Type': 'application/x-www-form-urlencoded; charset=utf-8'
		    }
		})
        .then(function(data){
            var html = data.data.replace(/img/g, 'pato');
			updateTempSchedule(html, callback);
        }, function(e){
			$scope.showGenericError(e);
        });
	};
	$scope.saveSchedule = function(){
		var d = new Date();
		var period = d.getFullYear() + '-' + (d.getMonth() > 5 ? 'II' : 'I');
		$http({
			method: 'POST',
			url: $scope.server + '/mEstudiantes/guardarHorario',
			data: $httpParamSerializerJQLike({
				codEst: $scope.user.code,
				periodo: period
			}),
			headers: {
		    	'Content-Type': 'application/x-www-form-urlencoded; charset=utf-8'
		    }
		})
        .then(function(data){
            var html = data.data.replace(/img/g, 'pato');
            $('#save').removeClass('show');
        }, function(e){
			$scope.showGenericError(e);
        });
	};
	$scope.cancelModal = function($event){
		var $modal = $($event.toElement).closest('.modal');
		$modal.attr('data-target', '');
	};
	$scope.openModalDelete = function($event){
		$event.preventDefault();
		$event.stopPropagation();
		var $c = $($event.toElement).closest('.course');
		$('#modalDelete .modal-content h4').text('¿Quieres eliminar ' + $c.find('.name').text().match(/[a-z]+ /)[0].replace(/ /, '') + '?');
		$('#modalDelete').attr('data-target', $c.attr('data-id'));
		$('#modalDelete').modal('open');
	};
	$scope.openModalSave = function(){
		$('#modalSave').modal('open');
	};
	$scope.deleteCourse = function(){
		var id = $('#modalDelete').attr('data-target');
		var $c = $('#semester .course[data-id="' + id + '"]');
		$c.css('height', $c.height() + 'px');
		$scope.semester.credits -= $c.find('.credits').text().match(/[0-9]+/)[0];
		$timeout(function(){
			$c.addClass('removed');
			$timeout(function(){
				$c.remove();
			}, 600);
		}, 100);
		$http({
			method: 'POST',
			url: $scope.server + '/mEstudiantes/delAsigTmp',
			data: $httpParamSerializerJQLike({
				jObj: id
			}),
			headers: {
		    	'Content-Type': 'application/x-www-form-urlencoded; charset=utf-8'
		    }
		})
        .then(function(data){
            var html = data.data.replace(/img/g, 'pato');
            updateTempSchedule(html);
        }, function(e){
            updateTempSchedule();
			$scope.showGenericError(e);
        });
	};
	/*var richardListo = false;
	var peticionTerminada = true;
	$interval(function(){
		if(activeCourse && !richardListo && peticionTerminada){
			peticionTerminada = false;
			$scope.updateGroups(activeCourse, function(){
				updateHeight(activeCourse);
				for (var i = 0; i < $scope.courses.length; i++) {
        			if($scope.courses[i].id == activeCourse){
        				for (var j = 0; j < $scope.courses[i].groups.length; j++) {
        					if($scope.courses[i].groups[j].number == 1){
        						console.log('Si hay');
        						$scope.addToSemester($scope.courses[i].groups[j].id, $scope.courses[i].groups[j].name, function(canSave){
        							if(canSave){
        								richardListo = true;
        								console.log('richardListo');
        								$scope.saveSchedule();
        							}
        						});
        					}
        				}
        				console.log('No hay');
        			}
            	}
				peticionTerminada = true;
			});
		}
	}, 20000);*/
}]);
