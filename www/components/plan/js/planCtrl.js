angular.module('starter').controller('planCtrl', ['$rootScope', '$scope', '$http', '$state', '$ionicPlatform', 'sesionFac', 'planFac', function($rootScope, $scope, $http, $state, $ionicPlatform, sesionFac, planFac){
	$scope.$on('$ionicView.loaded', function (viewInfo, state) {
        if(state.stateName == 'plan' && $scope.isLogged()){
            $scope.showLoading();
        	addPendingCourses(function(courses, error){
        		var container = [];
        		var separation = planFac.get().separation;
        		for (var i = 0; i < courses.length; i++) {
        			if(container.indexOf(courses[i][separation]) == -1)
        				container.push(courses[i][separation]);
        		}
        		for (var i = 0; i < container.length; i++) {
        			container[i] = {
        				name: container[i],
                		img: ($scope.icons.indexOf(container[i]) != -1 ?  container[i] : 'default') + '.svg',
        				courses: []
        			};
        			for (var j = 0; j < courses.length; j++) {
	        			if(courses[j][separation] == container[i].name)
	        				container[i].courses.push(courses[j]);
	        		}
        		}
        		container.separation = separation;
        		$scope.container = container;
                $scope.hideLoading();
                if(sesionFac.getUser().planVisited == undefined || !sesionFac.getUser().planVisited){
                    var u = sesionFac.getUser();
                    u.planVisited = true;
                    sesionFac.setUser(u);
                    $('.tap-target.plan-notice').tapTarget('open');
                }
        	});
		}
	});
    $scope.canSelect = false;
    $scope.addToSelection = function($event){
        if($scope.canSelect){
            $($event.currentTarget).closest('.course').toggleClass('selected');
        }
    }
    $scope.activeSelection = function($event){
        $scope.canSelect = true;
        var $v = $($event.currentTarget).addClass('selected').closest('.vista');
        $v.addClass('selection-active');
        $v.find('.btn-floating').removeClass('selection-active').addClass('done')
    }
    $scope.actionClick = function($event){
        var $btn = $($event.currentTarget).siblings('.btn-floating');
        if($btn.hasClass('selection-active')){
            $scope.canSelect = true; 
            $btn.removeClass('selection-active').addClass('done').closest('.vista').addClass('selection-active');
        }else{
            if($('.course.selected').length> 0){
                $btn.trigger('click');
            }else{
                $scope.canSelect = false;
                $('.btn-floating').removeClass('done').addClass('selection-active').closest('.vista').removeClass('selection-active');
            }
        }
    }
    $scope.cancelSelection = function($event){
        $('.course.selected').removeClass('selected').closest('.vista').removeClass('selection-active');
        $scope.canSelect = false;
        $('.btn-floating').removeClass('done').addClass('selection-active').trigger('click');
    }
    $scope.addToNext = function($event){
        $('.course.selected').each(function(){
            sesionFac.addToNextSemester(planFac.getCourse($(this).attr('data-name')));
        });
        $('.course.selected').removeClass('selected').closest('.vista').removeClass('selection-active');
        $scope.canSelect = false;
        $('.btn-floating').removeClass('done').addClass('selection-active').trigger('click');
    }
	function addPendingCourses(callback){
		sesionFac.prepare(function(data, error){
            if(error){
                callback(planFac.get().courses, error);
            }else{
                $http({
                    method: 'GET',
                    url: $scope.server + '/mEstudiantes/miMateriasPendientes.jsp',
                    headers: {
                        'Content-Type': 'application/x-www-form-urlencoded; charset=utf-8'
                    }
                })
                .then(function(data){
                    var html = data.data.replace(/img/g, 'pato');
                    var p = planFac.get();
                   	var courses = p.courses;
                    $(html).find('table.tbMatPendientes tr[align!="center"]').each(function(i, tr){
                    	var code = $($(tr).children()[1]).text().trim();
                    	for (var j = 0; j < courses.length; j++) {
                    		if(courses[j].code == code){
                    			courses[j].pending = true;
                    			if($(tr).hasClass('fondoYellow'))
                    				courses[j].studying = true;
                    			break;
                    		}
                    	}
                    });
                    planFac.set(p);
                    callback(courses);
                }, function(e){
                    callback(planFac.get().courses, e);
                });
            }
        });
	}
}]);