angular.module('starter').controller('historyCtrl', ['$scope', '$state', '$http', 'sesionFac', function($scope, $state, $http, sesionFac){
	$scope.semesters = [];
	$scope.$on('$ionicView.loaded', function (viewInfo, state) {
		if(state.stateName == 'history'){
            $scope.showLoading();
			getHistory(function(semesters, error){
				$scope.semesters = semesters;
            	$scope.hideLoading();
			});
		}
	});
	function getHistory(callback){
		sesionFac.prepare(function(){
			$http({
				method: 'GET',
				url: $scope.server + '/mEstudiantes/miSabana.jsp',
				headers: {
					'Content-Type': 'application/x-www-form-urlencoded; charset=utf-8'
				}
			})
			.then(function(data){
				var semesters = [];
				var html = data.data.replace(/img/g, 'pato');
				if($(html).find('table td.tbSitPro')[0]){
					$(html).find('table td.tbSitPro').each(function(){
						var $tr = $(this).closest('tr');
						var name = $tr.find('h4').text().trim().replace(/.+: /, '');
						var courses = [];
						var average;
						$tr.nextAll().each(function(){
							var $t = $(this);
							if($t.find('th').length == 0){
								if($t.find('td').length == 1){
									average = $t.find('td').text().trim().replace(/.+: /, '');
									return false;
								}else{
									var mode;
									switch($t.find('td:nth-child(5)').text().trim()){
										case 'NOR': 
											mode = 'Normal';
											break;
										case 'VAC': 
											mode = 'Vacacional';
											break;
										default: 
											mode = $t.find('td:nth-child(5)').text().trim();
											break;
									}
									courses.push({
										code: $t.find('td:nth-child(1)').text().trim(),
										name: $t.find('td:nth-child(2)').text().trim(),
										grade: $t.find('td:nth-child(3)').text().trim() != '-' ? $t.find('td:nth-child(3)').text().trim() : null,
										credits: $t.find('td:nth-child(4)').text().trim(),
										mode: mode,
										approved: $t.find('td:nth-child(6) pato').attr('src').search('ok') != -1,
										pending: $t.find('td:nth-child(6) pato').attr('src').search('alert') != -1
									});
								}
							}
						});
						var average_simbol = ':';
						if(average == '-'){
							var grades = 0;
							var credits = 0;
							var pending = false;
							for (var i = 0; i < courses.length; i++) {
								if(!courses[i].pending){
									credits += parseInt(courses[i].credits);
									grades += parseInt(courses[i].credits) *  parseInt(courses[i].grade);
								}else{
									pending = true;
									break;
								}
							}
							if(!pending){
								average_simbol = '≈';
								average = Math.round(grades/credits);
							}
							else
								average = null;
						}

						semesters.push({
							name: name,
							average: average,
							average_simbol: average_simbol,
							courses: courses
						});
					});
					callback(semesters);
				}else{
					callback(null, 'error');					
				}
			}, function(e){
				callback(null, e);
			});
		});
	}
}]);