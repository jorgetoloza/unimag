angular.module('starter').controller('nextSemesterCtrl', ['$rootScope', '$scope', '$http', '$state', '$ionicPlatform', 'sesionFac', 'planFac',  'focus', function($rootScope, $scope, $http, $state, $ionicPlatform, sesionFac, planFac, focus){
	$scope.semester = {courses: []};
	$scope.courseActive;
    $scope.$on('$ionicView.loaded', function (viewInfo, state) {
        if(state.stateName == 'next-semester'){
        	$('.dropdown-button').dropdown();
            $scope.showLoading();
            $scope.semester.courses = sesionFac.getNextSemester();
            var credits = 0;
            for (var i = 0; i < $scope.semester.courses.length; i++) {
            	credits += parseInt($scope.semester.courses[i].credits);
            }
            $scope.semester.credits = credits;
            $scope.hideLoading();
		}
	});
	$scope.remove = function($event){
		var $c = $($event.currentTarget).closest('.course');
		sesionFac.removeFromNextSemester($c.attr('data-name'));
		$c.css('height', $c.height() + 'px');
		$scope.semester.credits -= $c.find('.credits').text().match(/[0-9]+/)[0];
		setTimeout(function(){
			$c.addClass('removed');
			setTimeout(function(){
				$c.remove();
			}, 600);
		}, 100);
	}
	$scope.showMenu = function($event){
		if($(event.target).closest('.remove').length == 0){
			$scope.courseActive = $($event.currentTarget).closest('.course');
			$('.vista[name="next-semester"] .dropdown-button').dropdown('close');
			setTimeout(function(){
				$('.vista[name="next-semester"] .dropdown-button').dropdown('open');
				$('.vista[name="next-semester"] .dropdown-content').css({
					top: $event.gesture.center.pageY - 40,
					left: $event.gesture.center.pageX - 40
				});
			}, 100);
		}
	}
	$scope.addGroup = function($event){
		$scope.courseActive.find('.group').removeClass('hide');
		focus($scope.courseActive.find('.group input').attr('focus-on'));
	}
	$scope.addTeacher = function($event){
		$scope.courseActive.find('.teacher').removeClass('hide');
		focus($scope.courseActive.find('.teacher input').attr('focus-on'));
	}
	$scope.$watch('semester.courses', function (newValue, oldValue) {
		if(newValue == oldValue)
			return;
        sesionFac.setNextSemester($scope.semester.courses);
    }, true);
}]);
angular.module('starter').directive('focusOn', function() {
	return function(scope, elem, attr) {
		scope.$on('focusOn', function(e, name) {
			if(name === attr.focusOn) {
				elem[0].focus();
			}
		});
	};
});

angular.module('starter').factory('focus', function ($rootScope, $timeout) {
	return function(name) {
		$timeout(function (){
			$rootScope.$broadcast('focusOn', name);
		});
	}
});