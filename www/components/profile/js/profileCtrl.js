angular.module('starter').controller('profileCtrl', ['$scope', '$state', '$http', 'sesionFac', '$httpParamSerializerJQLike', 'planFac', function($scope, $state, $http, sesionFac, $httpParamSerializerJQLike, planFac){
	$scope.info = {};
    $scope.$on('$ionicView.loaded', function (viewInfo, state) {
        if(state.stateName == 'profile' && $scope.isLogged()){
            $scope.showLoading();
        	getInfo(function(info){
                $scope.info = info;
                var credits = planFac.get().credits;
                $scope.info.porcent = info.credits.approved / credits;
                $scope.info.porcent_human = Math.round($scope.info.porcent * 100);                
                $scope.info.remaining = info.credits.pending / 21;
                $scope.info.remaining_human = Math.ceil($scope.info.remaining);
            	$scope.hideLoading();
            });
		}
	});
	function getInfo(callback){
		// no necesita un sesion (no hay que llamar a prepare)
		$http({
			method: 'POST',
			url: $scope.server + '/mEstudiantes/ajaxPages/estadoEstudiante.jsp',
			data: $httpParamSerializerJQLike({codEst: $scope.user.code}),
			headers: {
		    	'Content-Type': 'application/x-www-form-urlencoded; charset=utf-8'
		    }
		})
        .then(function(data){
            var html = data.data.replace(/img/g, 'pato');
            var info = {courses: {}, credits: {}};
            try{
				info.program = $(html).find('th:contains("Programa:")').next().text().trim();
				info.plan_number = $(html).find('th:contains("Plan de estudio:")').next().text().trim();
				info.situation = $(html).find('th:contains("Situación Académica:")').next().text().trim();
				info.agreement = $(html).find('th:contains("Acuerdo Liquidación:")').next().text().trim();

				var $table = $(html).find('div:contains("Estado Académico")').next().find('table:first-child');
				info.courses.pending = $table.find('td:nth-child(1)')[0].childNodes[1].nodeValue.trim();
				info.credits.pending = $table.find('td:nth-child(2)')[0].childNodes[1].nodeValue.trim();

				info.courses.registered = $table.find('td:nth-child(3)')[0].childNodes[1].nodeValue.trim();
				info.credits.registered = $table.find('td:nth-child(4)')[0].childNodes[1].nodeValue.trim();

				$table = $(html).find('div:contains("Estado Académico")').next().find('table:last-child');

				info.courses.normal = $table.find('tr:nth-child(2) td:nth-child(2)').text().trim();
				info.credits.normal = $table.find('tr:nth-child(3) td:nth-child(2)').text().trim();

				info.courses.homologated = $table.find('tr:nth-child(2) td:nth-child(3)').text().trim();
				info.credits.homologated = $table.find('tr:nth-child(3) td:nth-child(3)').text().trim();

				info.courses.vacation = $table.find('tr:nth-child(2) td:nth-child(4)').text().trim();
				info.credits.vacation = $table.find('tr:nth-child(3) td:nth-child(4)').text().trim();

				info.courses.val = $table.find('tr:nth-child(2) td:nth-child(5)').text().trim();
				info.credits.val = $table.find('tr:nth-child(3) td:nth-child(5)').text().trim();

				info.courses.rec = $table.find('tr:nth-child(2) td:nth-child(6)').text().trim();
				info.credits.rec = $table.find('tr:nth-child(3) td:nth-child(6)').text().trim();

				info.courses.cdi = $table.find('tr:nth-child(2) td:nth-child(7)').text().trim();
				info.credits.cdi = $table.find('tr:nth-child(3) td:nth-child(7)').text().trim();

				info.courses.disapproved = $table.find('tr:nth-child(2) td:nth-child(8)').text().trim();
				info.credits.disapproved = $table.find('tr:nth-child(3) td:nth-child(8)').text().trim();

				info.courses.approved = $table.find('tr:nth-child(2) td:nth-child(9)').text().trim();
				info.credits.approved = $table.find('tr:nth-child(3) td:nth-child(9)').text().trim();
				callback(info);
			}catch(e){
				console.log(e);
				callback(null, e);				
			}
        }, function(e){
			callback(null, e);
        });
	}
}]);